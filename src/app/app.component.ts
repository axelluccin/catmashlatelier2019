import {Component, OnInit} from '@angular/core';
import {ChatsDataStorageService} from './services/chats-data-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private chatStorageData: ChatsDataStorageService) {}

  ngOnInit(): void {
    this.chatStorageData.remiseAZero().subscribe();
  }
}
