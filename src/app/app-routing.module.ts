import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChatVoteComponent} from './components/chat-vote/chat-vote.component';

const appRoutes: Routes = [
  { path: '', component: ChatVoteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
