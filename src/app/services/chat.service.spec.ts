import { TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';
import { ChatsDataStorageService } from './chats-data-storage.service';
import { Chat } from '../model/Chat.model';
import { of } from 'rxjs';


const listChat = [
  new Chat('zezfez.html', '564FD', 3),
  new Chat('zezfez.io', 'HF65D', 7),
  new Chat('zezfez.com', 'JKH78', 6653),
  new Chat('zezfez.jpeg', 'VCF32LOP', 79),
  new Chat('zezfez.html', 'jfg', 90),
  new Chat('zezfez.io', 'der', 54),
  new Chat('zezfez.com', 'qszer', 68753),
  new Chat('zezfez.jpeg', 'sdfr67', 234)
];

class ChatsDataStorageServiceStub {
  getListOfChat() { return of(listChat); }
  updateChat(chat: Chat) { return of(chat); }
}

describe('ChatService', () => {
  let serviceDataChat: ChatsDataStorageService;
  let serviceChat: ChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ChatService,
        { provide: ChatsDataStorageService, useClass: ChatsDataStorageServiceStub}
      ]
    });
    serviceChat = TestBed.get(ChatService);
    serviceDataChat = TestBed.get(ChatsDataStorageService);
  });

  it('should be created', () => {
    const service: ChatService = TestBed.get(ChatService);
    expect(service).toBeTruthy();
  });

  describe('#getChats', function () {
    it('should return a cat\'s list', function () {
      spyOn(serviceDataChat, 'getListOfChat').and.callThrough();
      serviceChat.getListChat();
      expect(serviceDataChat.getListOfChat).toHaveBeenCalled();
      expect(serviceChat.listChat).toBe(listChat);
      expect(serviceChat.listChat.length).toBe(8);
    });
  });

  describe('#updateListChat', function () {
    it('should update the cat\'s vote', function () {
      const updateChat = listChat[1];
      spyOn(serviceDataChat, 'updateChat').and.callThrough();
      serviceChat.updateOfChat(updateChat);
      expect(serviceDataChat.updateChat).toHaveBeenCalled();
    });
  });

  describe('#random chat', function () {
    it('should return 2 random cat', function () {
      serviceChat.listChat = listChat;
      const randomCats = serviceChat.randomCats();
      expect(randomCats.length).toBe(2);
      expect(randomCats[0]).not.toBe(randomCats[1]);
    });
  });
});
