import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Chat} from '../model/Chat.model';
import {Observable} from 'rxjs';

import data from '../data/data.json';

@Injectable({
  providedIn: 'root'
})
export class ChatsDataStorageService {
  API_URL = 'https://catmashatelier2019.firebaseio.com/chats' ;

  constructor(private http: HttpClient) { }

  /**
   * Permet de récuperer la liste de chat
   * @returns {Observable<Array<Chat>>}
   */
  getListOfChat(): Observable<Array<Chat>> {
    return this.http.get<Array<Chat>>(this.API_URL + '.json');
  }

  /**
   * Mise à jour d'un vote pour un chat
   * @param chat le chat
   * @param indice l'indice dans la liste de chat
   * @returns {Observable<Chat>} le chat mise à jour.
   */
  updateChat(chat: Chat, indice: number): Observable<Chat> {
    return this.http.patch<Chat>(this.API_URL + '/' + indice + '.json', chat);
  }

  /**
   * Permet de remettre à zéro la base de données.
   * @returns {Observable<Array<Chat>>} la liste des chats dans la base de données.
   */
  remiseAZero() {
    const chats: Array<Chat> = new Array<Chat>();
    for (const chatTemp of data.images) {
      chats.push(new Chat(chatTemp.url, chatTemp.id, 0));
    }
    return this.http.put<Array<Chat>>(this.API_URL + '.json', chats);
  }
}
