import { TestBed } from '@angular/core/testing';

import { ChatsDataStorageService } from './chats-data-storage.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Chat} from '../model/Chat.model';

const mockChats = [
  new Chat('test.html', 'sjfs78', 4),
  new Chat('test.io', 'fezf65R', 789)
];

describe('ChatsDataStorageService', () => {
  let httpTestingController: HttpTestingController;
  let chatDataStorageService: ChatsDataStorageService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatsDataStorageService],
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    chatDataStorageService = TestBed.get(ChatsDataStorageService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: ChatsDataStorageService = TestBed.get(ChatsDataStorageService);
    expect(service).toBeTruthy();
  });

  describe('#getChats', () => {
    it('should return the list of cat', () => {
      chatDataStorageService.getListOfChat().subscribe(
        (response: Array<Chat>) => {
          expect(response.length).toEqual(2);
          expect(response[0].id).toEqual('sjfs78');
          expect(response[1].vote).toEqual(789);
        }
      );
      const req = httpTestingController.expectOne(chatDataStorageService.API_URL + '.json');
      req.flush(mockChats);
    });
  });

  describe('#updateChat', () => {
    it('should udpate cat\'s vote', () => {
      const mockChat = new Chat('test.html', 'sjfs78', 4);

      mockChat.vote++;

      chatDataStorageService.updateChat(mockChat, 1).subscribe(
        (response: Chat) => {
          expect(response.vote).toEqual(5);
        });
      const req = httpTestingController.expectOne(chatDataStorageService.API_URL + '/1.json');
      req.flush(mockChat);
    });
  });

  describe('#remiseAZero', function () {
    it('should reset data in BDD', function () {
      chatDataStorageService.remiseAZero().subscribe(
        (response: Array<Chat>) => {
          expect(response).toEqual(mockChats);
        }
      );
      const req = httpTestingController.expectOne(chatDataStorageService.API_URL + '.json');
      req.flush(mockChats);
    });
  });
});
