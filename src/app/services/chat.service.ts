import { Injectable } from '@angular/core';
import { ChatsDataStorageService } from './chats-data-storage.service';
import {Chat} from '../model/Chat.model';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  listChat: Array<Chat> = new Array<Chat>();
  chatChanged: Subject<Array<Chat>> = new Subject<Array<Chat>>();

  constructor(private chatDataStorageService: ChatsDataStorageService) { }

  /**
   * Permet de récuperer la liste de chat.
   * @returns {Array<Chat>} la liste de chat.
   */
  getListChat() {
    this.chatDataStorageService.getListOfChat()
      .subscribe(
      (response: Array<Chat>) => {
        this.listChat = response;
        this.chatChanged.next(this.listChat);
      });
  }

  /**
   * Permet de mettre à jour le vote d'un chat.
   * @param chat le chat à mettre à jour.
   */
  updateOfChat(chat: Chat): void {
    chat.vote++;
    this.chatDataStorageService.updateChat(chat, this.listChat.indexOf(chat))
      .subscribe();
  }

  /**
   * Permet de récuperer deux chats aléatoirement.
   * return {Array<Chat>} les 2 chats aléatoire.
   */
  randomCats(): Array<Chat> {
    if (this.listChat === undefined) {
      this.getListChat();
    }
    const randomChat = new Array<Chat>();
    do {
      const chat = this.listChat[Math.floor(Math.random() * this.listChat.length)];
      if (randomChat.indexOf(chat) === -1) {
        randomChat.push(chat);
      }
    } while (randomChat.length !== 2);
    return randomChat;
  }
}
