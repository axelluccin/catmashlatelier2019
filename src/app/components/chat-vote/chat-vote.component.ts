import { Component, OnInit} from '@angular/core';
import {Chat} from '../../model/Chat.model';
import {ChatService} from '../../services/chat.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-chat-vote',
  templateUrl: './chat-vote.component.html',
  styleUrls: ['./chat-vote.component.css'],
  providers: [ChatService]
})
export class ChatVoteComponent implements OnInit {
  subscription: Subscription;
  randomCat: Array<any>;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.getListChat();
    this.subscription = this.chatService.chatChanged.subscribe(
      () => {
        this.randomCat = this.chatService.randomCats();
      }
    );
  }

  onClickChat(chat: Chat) {
    console.log(chat);
    this.chatService.updateOfChat(chat);
    this.randomCat = this.chatService.randomCats();
  }
}
