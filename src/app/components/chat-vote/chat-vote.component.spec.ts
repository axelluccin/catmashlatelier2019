import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { ChatVoteComponent } from './chat-vote.component';
import {ChatService} from '../../services/chat.service';
import {Chat} from '../../model/Chat.model';
import {Observable, Observer, of, Subject} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const mockListChat = [
  new Chat('http://24.media.tumblr.com/tumblr_m82woaL5AD1rro1o5o1_1280.jpg', '564FD', 3),
  new Chat('http://24.media.tumblr.com/tumblr_m29a9d62C81r2rj8po1_500.jpg', 'HF65D', 7),
  new Chat('http://25.media.tumblr.com/tumblr_m4bgd9OXmw1qioo2oo1_500.jpg', 'JKH78', 6653),
  new Chat('http://24.media.tumblr.com/tumblr_lzxok2e2kX1qgjltdo1_1280.jpg', 'VCF32LOP', 79)
];

class MockChatService {
  chatChanged: Subject<Array<Chat>> = new Subject<Array<Chat>>();
  chatState = this.chatChanged.asObservable();
  randomChat() {
    return mockListChat.slice(2, 3);
  }

  getListChat() {
    this.chatChanged.next(mockListChat);
  }
}

describe('ChatVoteComponent', () => {
  let component: ChatVoteComponent;
  let fixture: ComponentFixture<ChatVoteComponent>;
  let testBedChatService: ChatService;
  let componentService: ChatService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ ChatVoteComponent ],
      providers: [ChatService]
    });

    TestBed.overrideComponent(
      ChatVoteComponent,
      { set: { providers: [{ provide: ChatService, useClass: MockChatService}]}}
    );

    fixture = TestBed.createComponent(ChatVoteComponent);
    component = fixture.componentInstance;
    testBedChatService = TestBed.get(ChatService);
    componentService = fixture.debugElement.injector.get(ChatService);
  }));

  it('Service injected via inject(..) and TestBed.get(...) should be the same',
    inject([ChatService], (injectService: ChatService) => {
      expect(injectService).toBe(testBedChatService);
    })
  );

  it('Service injected via component should be and instance of MockChatService',  () => {
    expect(componentService instanceof MockChatService).toBeTruthy();
  });

  it('should return a random cats', function () {
  const mockChatService = new MockChatService();
    /*const fakeComponent = new ChatVoteComponent(mockChatService as any);
    fakeComponent.ngOnInit();
    mockChatService.getListChat();
    expect(fakeComponent.randomCat.length).toEqual(2);*/
  });
});
