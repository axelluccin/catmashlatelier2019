import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChatVoteComponent } from './components/chat-vote/chat-vote.component';
import {ChatService} from './services/chat.service';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {ChatsDataStorageService} from './services/chats-data-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    ChatVoteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ChatService, ChatsDataStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
