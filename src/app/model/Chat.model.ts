export class Chat {
  /**
   * Assigner le vote du chat.
   * @param value la valeur.
   */
  public setVote(value: number) {
    this.vote = value;
  }

  /**
   * Retourne l'url de l'image du chat.
   * @returns {string} l'url.
   */
  getUrl(): string {
    return this.url;
  }

  /**
   * Retourne l'id du chat.
   * @returns {string} l'id.
   */
  getId(): string {
    return this.id;
  }

  /**
   * Retourne le nombre de vote du chat.
   * @returns {number} le nombre de vote
   */
  getVote(): number {
    return this.vote;
  }

  /**
   * Constructeur de la classe Chat.
   * @param url l'url de l'image du chat.
   * @param id l'id du chat.
   * @param vote le nombre de vote du chat.
   */
  constructor(private url: string, public id: string, public vote: number) {}
}
